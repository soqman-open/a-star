﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathFinder : MonoBehaviour
{
	private Node[,] _nodes;
	private Node _nodeA;
	private Node _nodeB;

	public Node NodeA
	{
		get { return _nodeA; }
		set
		{
			if (_nodeA != null)_nodeA.SetStatus(_nodeA.Equals(_nodeB) ? NodeStatus.Goal : NodeStatus.Empty);
			_nodeA = value;
			_nodeA.SetStatus(NodeStatus.Start);
		}
	}

	public Node NodeB
	{
		get { return _nodeB; }
		set
		{
			if(_nodeB!=null)_nodeB.SetStatus(_nodeB.Equals(_nodeA) ? NodeStatus.Start : NodeStatus.Empty);
			_nodeB = value;
			_nodeB.SetStatus(NodeStatus.Goal);
		}
	}

	public Node[,] Nodes
	{
		get { return _nodes; }
		set { _nodes = value; }
	}
	
	private static int Distance(Node a, Node b)
	{
		return Math.Abs(a.X - b.X) + Math.Abs(a.Y - b.Y);
	}
	
	public void Search()
	{
		var frontier = new Queue<Node>();
		frontier.Enqueue(_nodeA);
		var visited = new HashSet<Node> {_nodeA};
		while (frontier.Count > 0)
		{
			var current = frontier.Dequeue();
			foreach (var item in GetNeighborsNodes(current))
			{
				if (visited.Contains(item))
				{
					continue;
				}
				if (item.Equals(_nodeB))
				{
					item.CameFrom = current;
					MarkPath();
					return;
				}
				frontier.Enqueue(item);
				visited.Add(item);
				item.CameFrom = current;
				
			}
		}
	}
	
	public void SearchStepByStep()
	{
		StartCoroutine(SearchStepByStepEnumerator());
	}
	
	private IEnumerator SearchStepByStepEnumerator()
	{
		var frontier = new Queue<Node>();
		frontier.Enqueue(_nodeA);
		while (frontier.Count > 0)
		{
			var current = frontier.Dequeue();
			if (current.Status.Equals(NodeStatus.Frontier))
			{
				if(!current.Equals(NodeA))current.SetStatus(NodeStatus.Visited);
			}
			foreach (var item in GetNeighborsNodes(current))
			{
				if (item.Equals(_nodeB))
				{
					item.CameFrom = current;
					MarkPath();
					yield break;
				}
				if (item.Status.Equals(NodeStatus.Frontier) || item.Status.Equals(NodeStatus.Visited))
				{
					continue;
				}		
				if(!item.Equals(NodeA))item.SetStatus(NodeStatus.Frontier);
				frontier.Enqueue(item);
				item.CameFrom = current;
				yield return null;
			}
		}
	}
	
	public void SearchAStar()
	{
		var cost = new Dictionary<Node, int>();
		var frontier = new PriorityQueue<Node>();
		frontier.Enqueue(NodeA, 0);
		cost[NodeA] = 0;
		while (frontier.Count > 0)
		{
			var current = frontier.Dequeue();
			foreach (var item in GetNeighborsNodes(current))
			{
				if (item.Equals(NodeB))
				{
					item.CameFrom = current;
					MarkPath();
					return;
				}
				var newCost = cost[current] + 1;
				if (cost.ContainsKey(item) && newCost >= cost[item]) continue;
				cost[item] = newCost;
				var priority = newCost + Distance(item, NodeB);
				frontier.Enqueue(item, priority);
				item.CameFrom = current;
			}
		}
	}
	
	public void SearchAStarStepByStep()
	{
		StartCoroutine(SearchAStarEnumerator());
	}
	
	private IEnumerator SearchAStarEnumerator()
	{
		var costSum = new Dictionary<Node, int>();
		var frontier = new PriorityQueue<Node>();
		frontier.Enqueue(NodeA, 0);
		costSum[NodeA] = 0;
		while (frontier.Count > 0)
		{
			var current = frontier.Dequeue();
			if (current.Status.Equals(NodeStatus.Frontier))
			{
				if(!current.Equals(NodeA))current.SetStatus(NodeStatus.Visited);
			}
			foreach (var item in GetNeighborsNodes(current))
			{
				if (item.Equals(NodeB))
				{
					item.CameFrom = current;
					MarkPath();
					yield break;
				}
				var newCost = costSum[current] + 1;
				if (costSum.ContainsKey(item) && newCost >= costSum[item]) continue;
				costSum[item] = newCost;
				var priority = newCost + Distance(item, NodeB);
				frontier.Enqueue(item, priority);
				item.CameFrom = current;
				if(!item.Equals(NodeA))item.SetStatus(NodeStatus.Frontier);
				yield return null;
			}
		}
		MarkPath();
	}
	
	private void MarkPath()
	{
		var current = NodeB;
		while (current != null && !current.Equals(NodeA))
		{
			current = current.CameFrom;
			if(current==null || current.Equals(NodeA) || current.Equals(NodeB)) continue;
			current.SetStatus(NodeStatus.PathItem);
		}
	}

	private IEnumerable<Node> GetNeighborsNodes(Node node)
	{
		var neighbors=new HashSet<Node>();
		var x = node.X;
		var y = node.Y;
		if (x >= 1 && Nodes[x - 1, y] != null && !Nodes[x - 1, y].Status.Equals(NodeStatus.Wall))
		{
			neighbors.Add(Nodes[x - 1, y]);
		}
		if (x < Nodes.GetLength(0)-1 && Nodes[x + 1, y] != null && !Nodes[x + 1, y].Status.Equals(NodeStatus.Wall))
		{
			neighbors.Add(Nodes[x + 1, y]);
		}
		if (y >= 1 && Nodes[x, y - 1] != null && !Nodes[x, y - 1].Status.Equals(NodeStatus.Wall))
		{
			neighbors.Add(Nodes[x, y - 1]);
		}
		if (y < Nodes.GetLength(0)-1 && Nodes[x, y + 1] != null && !Nodes[x, y + 1].Status.Equals(NodeStatus.Wall))
		{
			neighbors.Add(Nodes[x, y + 1]);
		}
		return neighbors;
	}
	
	public void Clean()
	{
		StopAllCoroutines();
		foreach (var node in _nodes)
		{
			if(node.IsEngaged()) continue;
			node.SetStatus(NodeStatus.Empty);
		}
	}

	private void OnDestroy()
	{
		StopAllCoroutines();
	}
}
