﻿using UnityEngine;

[RequireComponent(typeof(UnityEngine.UI.InputField))]
public class InputLimiter : MonoBehaviour
{
	private UnityEngine.UI.InputField _inputField;
	public IntegerValue Max;
	public IntegerValue Min;
	public int MaxOffset;
	public int MinOffset;
	private int _max;
	private int _min;
	
	private void Start()
	{
		_inputField = GetComponent<UnityEngine.UI.InputField>();
		_inputField.onValueChanged.AddListener(OnValueChanged);
		_max = Max != null ? Max.Value+MaxOffset : int.MaxValue;
		_min = Min != null ? Min.Value+MinOffset : int.MinValue;
	}

	private void OnDestroy()
	{
		if(_inputField!=null)_inputField.onValueChanged.RemoveListener(OnValueChanged);
	}

	private void OnValueChanged(string value)
	{
		int m;
		int.TryParse(value, out m);
		if (m > _max)
		{
			_inputField.onValueChanged.Invoke(_max.ToString());
			_inputField.text = _max.ToString();
		}
		int.TryParse(value, out m);
		if (m < _min)
		{
			_inputField.onValueChanged.Invoke(_min.ToString());
			_inputField.text = _min.ToString();
		}
	}
}
