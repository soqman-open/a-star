﻿using UnityEngine;
using UnityEngine.EventSystems;

public class UrlPlay : EventTrigger
{
    private string _url;
    private void Start()
    {
        _url = GetComponent<UnityEngine.UI.Text>().text;
    }

    public override void OnPointerClick(PointerEventData data)
    {
        Application.OpenURL(_url);
    }
}