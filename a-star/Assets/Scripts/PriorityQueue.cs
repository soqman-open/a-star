﻿using System.Collections.Generic;

public class PriorityQueue<T>
{
	private readonly List<Tuple<T, int>> _items = new List<Tuple<T, int>>();

	public int Count
	{
		get { return _items.Count; }
	}
    
	public void Enqueue(T item, int priority)
	{
		_items.Add(Tuple.Create(item, priority));
	}

	public T Dequeue()
	{
		var top = 0;
		for (var i = 0; i < _items.Count; i++) 
		{
			if(_items[i].Item2 < _items[top].Item2) 
			{
				top = i;
			}
		}
		var first = _items[top].Item1;
		_items.RemoveAt(top);
		return first;
	}
}
