﻿using System;
using JetBrains.Annotations;
using UnityEngine;

[RequireComponent(typeof(RectTransform))]

public class Node : MonoBehaviour
{
    [Header("Info")]
    public int X;
    public int Y;
    public Node CameFrom;
    public NodeStatus Status { get; private set; }
    [Header("Settings")]
    [UsedImplicitly]public Color EmptyColor=new Color(0.76f,0.76f,0.76f);
    [UsedImplicitly]public Color WallColor=Color.magenta;
    [UsedImplicitly]public Color StartColor=new Color(1f,0.5f,0f);
    [UsedImplicitly]public Color GoalColor=Color.red;
    [UsedImplicitly]public Color PathColor=new Color(0.5f,1f,0.5f);
    [UsedImplicitly]public Color FrontierColor=Color.cyan;
    [UsedImplicitly]public Color VisitedColor=Color.yellow;

    public void SetStatus(NodeStatus status)
    {
        var image=GetComponent<UnityEngine.UI.Image>();
        switch (status)
        {
            case NodeStatus.Empty:
                image.color = EmptyColor;
                break;
            case NodeStatus.Wall:
                image.color = WallColor;
                break;
            case NodeStatus.Start:
                image.color = StartColor;
                break;
            case NodeStatus.Goal:
                image.color = GoalColor;
                break;
            case NodeStatus.PathItem:
                image.color = PathColor;
                break;
            case NodeStatus.Frontier:
                image.color = FrontierColor;
                break;
            case NodeStatus.Visited:
                image.color = VisitedColor;
                break;
            default:
                throw new ArgumentOutOfRangeException("status", status, null);
        }
        Status = status;
    }
    

    public void OnPointerDown()
    {
        if (Status==NodeStatus.Goal || Status==NodeStatus.Start) return;
        if (IsEngaged())
        {
            DrawStateHandler.SetEraseMode();
            SetIsEmpty();
        }
        else
        {
            DrawStateHandler.SetDrawMode();
            SetIsEngaged();
        }   
    }

    public void OnPointerEnter()
    {
        if (Status==NodeStatus.Goal || Status==NodeStatus.Start) return;
        if (!Input.GetMouseButton(0)) return;
        if (DrawStateHandler.GetDrawMode() == DrawStateHandler.DrawMode.Draw && !IsEngaged())
        {
            SetIsEngaged();
        }
        else if (DrawStateHandler.GetDrawMode() == DrawStateHandler.DrawMode.Erase && IsEngaged())
        {
            SetIsEmpty();
        }
    }

    private void SetIsEngaged()
    {
        SetStatus(NodeStatus.Wall);
    }

    private void SetIsEmpty()
    {
        SetStatus(NodeStatus.Empty);
    }

    public bool IsEngaged()
    {
        return Status.Equals(NodeStatus.Wall) || Status.Equals(NodeStatus.Goal) || Status.Equals(NodeStatus.Start);
    }
}
