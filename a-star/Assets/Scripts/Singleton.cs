﻿using UnityEngine;

    public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static T _instance;
        private static bool _isDestroyed;
        public static T Instance
        {
            get
            {
                if (_instance != null) return _instance;
                _instance = (T) FindObjectOfType(typeof(T));
                if (_instance != null) return _instance;
                if (_isDestroyed) return null;
                var o = new GameObject(typeof(T).Name);
                _instance = o.AddComponent<T>();
                return _instance;
            }
        }

        private void OnDestroy()
        {
            _isDestroyed = true;
        }
    }