﻿public static class DrawStateHandler
{
	public enum DrawMode
	{
		Draw,
		Erase
	}
	
	private static DrawMode _drawMode=DrawMode.Draw;

	public static void SetEraseMode()
	{
		_drawMode = DrawMode.Erase;
	}
	
	public static void SetDrawMode()
	{
		_drawMode = DrawMode.Draw;
	}

	public static DrawMode GetDrawMode()
	{
		return _drawMode;
	} 
}
