﻿using System;
using JetBrains.Annotations;
using UnityEngine;

public class Area : Singleton<Area>
{
    [Header("Settings")]
    [UsedImplicitly] public IntegerValue Size;
    [Header("GameObjects")]
    [UsedImplicitly] public Node Node;
    [UsedImplicitly] public RectTransform AreaTransform;

    private static Node[,] _nodes;
    private PathFinder _pathFinder;
    private int _size;
    
    private void Start()
    {
        _size = Size.Value;
        _nodes=new Node[_size, _size];
        ChangeNodeSize();
        InstantiateNodes();
        InitPathFinder();
    }

    private void ChangeNodeSize()
    {
        var areaSize = AreaTransform.sizeDelta.x;
        var nodeSizeDefined = areaSize / _size;
        ((RectTransform)Node.transform).sizeDelta=new Vector2(nodeSizeDefined,nodeSizeDefined);
    }
    
    private void InstantiateNodes()
    {
        var nodeSize = ((RectTransform)Node.transform).sizeDelta.x;
        var areaSize = AreaTransform.sizeDelta.x;
        var offset = -(areaSize / 2-nodeSize / 2);
        
        for (var i = 0; i < _size; i++)
        {
            for (var j = 0; j < _size; j++)
            {
                var position = new Vector3(i*nodeSize+offset,j*nodeSize+offset,0);
                var node = Instantiate(Node.gameObject, AreaTransform.position+position, Quaternion.identity, AreaTransform).GetComponent<Node>();
                node.X = i;
                node.Y = j;
                _nodes[i, j] = node.GetComponent<Node>();
            }
        }
    }

    private void InitPathFinder()
    {
        _pathFinder=gameObject.AddComponent<PathFinder>();
        _pathFinder.Nodes = _nodes;
        _pathFinder.NodeA = _nodes[0, 0];
        _pathFinder.NodeB = _nodes[_size - 1, _size - 1];
    }

    public void Search()
    {
        _pathFinder.Clean();
        _pathFinder.SearchAStar();
    }
    
    public void SearchVisible()
    {
        _pathFinder.Clean();
        _pathFinder.SearchAStarStepByStep();
    }

    public void Clean()
    {
        _pathFinder.StopAllCoroutines();
        foreach (var node in _nodes)
        {
            if (node.Status.Equals(NodeStatus.Goal) || node.Status.Equals(NodeStatus.Start)) continue;
            node.SetStatus(NodeStatus.Empty);
        }
    }

    private void SetGoal(int x, int y)
    {
        if (_pathFinder != null)
        {
            _pathFinder.NodeB = _nodes[x, y];
        }
    }
    
    private void SetStart(int x, int y)
    {
        if (_pathFinder != null)
        {
            _pathFinder.NodeA = _nodes[x, y];
        }
    }

    public void SetGoalX(string x)
    {
        int value;
        if (!int.TryParse(x, out value) || value<0||value>_size-1 || _pathFinder == null) return;
        SetGoal(value,_pathFinder.NodeB.Y);
    }
    
    public void SetGoalY(string y)
    {
        int value;
        if (!int.TryParse(y, out value) || value<0||value>_size-1 || _pathFinder == null) return;
        SetGoal(_pathFinder.NodeB.X,value);
    }
    
    public void SetStartX(string x)
    {
        int value;
        if (!int.TryParse(x, out value) || value<0||value>_size-1 || _pathFinder == null) return;
        SetGoal(value,_pathFinder.NodeA.Y);
    }
    
    public void SetStartY(string y)
    {
        int value;
        if (!int.TryParse(y, out value) || value<0||value>_size-1 || _pathFinder == null) return;
        SetGoal(_pathFinder.NodeA.X,value);
    }
}
