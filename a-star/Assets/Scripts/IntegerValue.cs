﻿using UnityEngine;
[CreateAssetMenu(fileName = "NewIntegerValue", menuName = "Int Value", order = 51)]

public class IntegerValue : ScriptableObject
{
	public int Value;
}
